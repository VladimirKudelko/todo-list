const express  = require('express');
const router   = express.Router();
const jwt      = require('jsonwebtoken');
const passport = require("passport");

const jwtsecret     = "mysecretkey"; // ключ для подписи JWT

/* POST login. */
router.post('/login', (req, res, next) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: 'Something is not right',
                user   : user
            });
        }

        const token = "bearer " + jwt.sign(user.toJSON(), jwtsecret, {
            expiresIn: 604800 
        });
        return res.json({ user, token });

    })(req, res);
});

/* GET login/google */
router.get('/login/googleplus', passport.authenticate('google', {
    scope: ['profile'],
}));

/* Callback route for google redirect to */
router.get('/login/googleplus/redirect', passport.authenticate('google'), (req, res) => {
    res.send("You reached the callback uri");
});

module.exports = router;