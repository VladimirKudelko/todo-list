const express  = require('express');
const router   = express.Router();
const passport = require('passport');

// Require controllers
const user_controller = require('../controllers/userController');

function checkUser(req, res, next) {
    passport.authenticate('jwt', (err, user) => {
        if (!user || !err == null) {
            return res.status(401).json({
                errorMessage: 'You are not authorized'
            });    
        }

        req.user = user;
        next();
    })(req, res, next);
}

/// USER ROUTES ///
router.get('/notes', checkUser, user_controller.user_notelist_get);
 
router.post('/notes', checkUser, user_controller.user_create_note_post);

router.delete('/notes/:id', checkUser, user_controller.user_delete_note_delete);

router.get('/users', checkUser, user_controller.user_list_get);

router.delete('/users/:id', checkUser, user_controller.user_remove_delete);

router.post("/reg", user_controller.user_signup_post);

module.exports = router;