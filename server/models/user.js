const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

mongoose.plugin(uniqueValidator);

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: 'username is required',
        unique: 'username must be unique',
    },
    password: {
        type: String,
        required: true
    },
    listnotes: {
        type: Array,
        default: []
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('User', UserSchema);