const express       = require('express');
const bodyParser    = require('body-parser'); // middleware - промежуточный обработчик
const db            = require('./utils/DataBaseUtils');
const cors          = require('cors'); // middleware - промежуточный обработчик
const config        = require('../etc/config.json');
const errorHandlers = require('./errorHandlers');
const userRoutes    = require('./routes/users');

require('./passport');
const auth = require('./routes/auth');

const app = express();
db.setUpConnection();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use((req, res, next) => { console.log(`${req.method.toUpperCase()}: ${req.url}`); next(); });
app.use(userRoutes);
app.use(auth);
app.use(errorHandlers.logErrors);
app.use(errorHandlers.clientErrorHandler);
app.use(errorHandlers.errorHandler);

const server = app.listen(8080, () => {
    console.log(`Server is running on port ${config.serverPort}`);
});