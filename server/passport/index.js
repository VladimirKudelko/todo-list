const passport       = require('passport');
const LocalStrategy  = require('passport-local').Strategy;
const passportJWT    = require("passport-jwt");
const JWTStrategy    = passportJWT.Strategy;
const ExtractJWT     = passportJWT.ExtractJwt;
// const GoogleStrategy = require('passport-google-oauth2').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
const bcrypt         = require('bcrypt');

const jwtsecret      = "mysecretkey"; // ключ для подписи JWT
const User           = require('../models/user');

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, 
    (username, password, cb) => {
        //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
        return User.findOne({ username })
           .then(user => {
                if (!user) {
                    return cb(null, false, { message: 'Incorrect username or password.' });
                }
                
                return Promise.resolve(user);
            })
            .then(user => {
                return bcrypt.compare(password, user.password)
                    .then(data => {
                        if (!data) {
                            return Promise.reject({ message: 'Auth is failed' });
                        }

                        return cb(null, user, { message: 'Logged In Successfully' });
                    })
            })
            .catch(err => cb(err));
    }
));

passport.use('jwt', new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : jwtsecret
    },
    (jwtPayload, cb) => {
        //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
        return User.findById(jwtPayload._id)
            .then(user => {
                return cb(null, user);
            })
            .catch(err => {
                return cb(err);
            });
    }
));

// Use the GoogleStrategy within Passport. Strategies in Passport require a `verify` function, which accept
// credentials (in this case, an accessToken, refreshToken, and Google profile), and invoke a callback with a user object.
passport.use(new GoogleStrategy({
        clientID: '310726796521-a0jv67gt0g84v3pmbpl5n416nost69ei.apps.googleusercontent.com',
        clientSecret: 'RfviTUbRunSagVO2pzOzb5hy',
        callbackURL: "/login/googleplus/redirect"
    },
    (accessToken, refreshToken, profile, done) => {
        console.log('this is a passport callback function')
        // User.findOne({ googleId: profile.id }, (err, user) => { // findOrCreate
        //     return done(err, user);
        // });
    }
));