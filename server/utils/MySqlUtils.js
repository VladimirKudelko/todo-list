const Sequelize = require('sequelize');

const config    = require('../../etc/config.json');

const connection = new Sequelize(config.db[1].nameDB, config.db[1].username, config.db[1].password, {
    host: config.db[1].host,
    dialect: 'mysql'
});

const User = connection.define('users', { // Создали модель
    username: {
        type: Sequelize.STRING,
        unique: true,
    },
    password: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
},
{
    timestamps: false
});

const Note = connection.define('notes', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
},
{
    timestamps: false
});

User.hasMany(Note, { targetKey: 'userId', sourceKey: 'id' });

function setUpConnection() {
    connection.sync()
        .then(() => { console.log("SUCCESS CONNECT TO DB"); })
        .catch(() => { console.log("FAILED CONNECT TO DB"); }); // Синхронизировали все модели
}

/* Notes */
function listNotes(user) {
    return Note.findAll({
        where: {
            userId: user.id
        }
    });
}

function createNote(data, user) {
    const instance = Note.build({
        id: data._id,
        name: data.name,
        userId: user.id
    });

    return instance.save();
}

function deleteNote(id) {
    console.log(id);
    return Note.destroy({
        where: {
            id: id
        }
    });
}

/* Users */
function listUsers() {
    return User.findAll();
}

function createUser(data) {
    return User.create({
        username: data.username.trim(),
        password: data.password
    });
}

function deleteUser(id) {
    return User.destroy({
        where: {
            id: id
        }
    });
}

function findByUsername(username) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            User.findOne({ where: { username } })
                .then(data => {
                    resolve(data.dataValues);
                })
                .catch(err => {
                    reject(err);
                })
        }, 0);
    });
}

module.exports = {
    User,
    setUpConnection,
    listNotes,
    createNote,
    deleteNote,
    listUsers,
    createUser,
    deleteUser,
    findByUsername
}