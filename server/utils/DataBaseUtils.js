const mongoose = require('mongoose');
const User     = require('../models/user');
const config   = require('../../etc/config.json');

function setUpConnection() {
    mongoose.connect(`mongodb://${config.db[0].host}:${config.db[0].port}/${config.db[0].name}`);
}

/* Notes */
function listNotes(user) {
    return User.findOne({ username: user.username })
        .then(data => {
            return data.listnotes
        })
        .catch(err => {
            return err
        });
}

function createNote(data, user) {    
    const note = {
        _id: data._id,
        name: data.name
    };

    return User.update(
        { username: user.username },
        { $push: { listnotes: note } },
    );
}

function deleteNote(id, user) {
    return User.update(
        { username: user.username },
        { $pull: { 'listnotes': { _id: id } } }
    );
}

/* Users */
function listUsers() {
    return User.find();
}

function createUser(data) {
    const user = new User({
        username: data.username.trim(),
        password: data.password,
        listnotes: []
    });

    return user.save();
}

function deleteUser(id) {
    return User.findById(id).remove();
}

function findByUsername(username) {
    return User.findOne({ username });
}

module.exports = {
    setUpConnection,
    listNotes,
    createNote,
    deleteNote,
    listUsers,
    createUser,
    deleteUser,
    findByUsername
};
