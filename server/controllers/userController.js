const jwt       = require('jsonwebtoken');
const bcrypt    = require('bcrypt');

const jwtsecret = "mysecretkey";
const User      = require('../models/user');
const db        = require('../utils/DataBaseUtils');
// const User      = require('../utils/MySqlUtils').User;
// const db        = require('../utils/MySqlUtils');

function generateToken(user) { 
    let u = {
        id: user.id,
        isAdmin: user.isAdmin,
        username: user.username,
    }

    return token = "bearer " + jwt.sign(u, jwtsecret, { expiresIn: 60 * 60 * 24 });
}

// Display user's notelist
function user_notelist_get(req, res) {
    db.listNotes(req.user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.json({
                errorMessage: err
            });
        });
}

// Add note to the user
function user_create_note_post(req, res) {
    db.createNote(req.body, req.user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.json({
                errorMessage: err
            });
        });
}

// Delete note in private user's listnote
function user_delete_note_delete(req, res) {
    db.deleteNote(req.params.id, req.user)
        .then(data => {
            res.sendStatus(200);
        })
        .catch(err => {
            res.json({
                errorMessage: err
            });
        });
}

// Display all users
function user_list_get(req, res) {
    db.listUsers()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.json({
                errorMessage: err
            });
        });
}

// Delete one user
function user_remove_delete(req, res) {
    db.deleteUser(req.params.id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.json({
                errorMessage: err
            });
        });
}

// Sign up user
function user_signup_post(req, res) {   
    let body = req.body;

    body.password = bcrypt.hashSync(body.password.trim(), 10);
    
    db.createUser(body)
        .then(data => {
            res.json({
                "token": generateToken(data),
                "user": data
            });
        })
        .catch(err => {
            res.json({
                errorMessage: err.message
            });
        });
}

module.exports = {
    user_notelist_get,
    user_create_note_post,
    user_delete_note_delete,
    user_list_get,
    user_remove_delete,
    user_signup_post
};