import axios from "./axiosWrapper";

import { apiPrefix } from "../../etc/config.json";

export default {
    listNotes() {
        return axios.get(`${apiPrefix}/notes`);
    },
    createNote(data) {
        return axios.post(`${apiPrefix}/notes`, data);
    },
    deleteNote(noteId) {
        return axios.delete(`${apiPrefix}/notes/${noteId}`);
    }, 
    signUpUser(data) {
        return axios.post(`${apiPrefix}/reg`, data);
    }, 
    logInUser(data) {
        return axios.post(`${apiPrefix}/login`, data);
    },
    logInUserGoogle() {
        return axios.get(`${apiPrefix}/login/googleplus`);
    },
    usersList() {
        return axios.get(`${apiPrefix}/users`);
    },
    deleteUser(userId) {
        return axios.delete(`${apiPrefix}/users/${userId}`);
    }
}