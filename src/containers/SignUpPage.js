import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { asyncSignUp } from "../actions/users";

class SignUpPage extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        const username = this.username.value;
        const password = this.password.value;
        const passwordRepeat = this.passwordRepeat.value;

        password === passwordRepeat ? 
            this.props.onSignUp({ username, password }) : 
            alert("Passwords do not match");
    }

    render() {
        return (
            <div className="signup-form">
                <form onSubmit={ this.onSubmit }>
                    <input type="text" placeholder="username" ref={ (input) => this.username = input } required autoFocus={true}/>
                    <input type="password" placeholder="password" ref={ (input) => this.password = input } required/>
                    <input type="password" placeholder="password repeat" ref={ (input) => this.passwordRepeat = input } required/>
                    <Link to="/"><button>Home</button></Link>
                    <button type="submit">Sign Up</button>
                </form>
            </div>);
    }
}

const mapDispatchToProps = dispatch => ({
    onSignUp(data) {
        dispatch(asyncSignUp(data));
    }
});

export default connect(null, mapDispatchToProps, null, { pure: false })(SignUpPage);