import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { asyncLogIn, asyncLogInGoogle } from "../actions/users";

class LogInPage extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit (event) {
        event.preventDefault();

        const username = this.username.value;
        const password = this.password.value;

        this.props.onLogIn({ username, password });
    }

    render() {
        return (
            <div>
                <div className="login-form">
                    <form onSubmit={ this.onSubmit }>
                        <input type="text" placeholder="username" ref={ input => this.username = input } required/>
                        <input type="password" placeholder="password" ref={ input => this.password = input } required/>
                        <Link to="/"><button>Home</button></Link>
                        <button type="submit">Login</button>
                    </form>
                </div>
                <div className="auth-via-social-networks">
                    <h1>Authorization via</h1>
                    <button onClick={ this.props.onLogInGoogle } className="gooogle-plus-btn">Google+</button>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onLogIn(data) {
        dispatch(asyncLogIn(data));
    },
    onLogInGoogle() {
        dispatch(asyncLogInGoogle());
    }
});

export default connect(null, mapDispatchToProps, null, { pure: false })(LogInPage);