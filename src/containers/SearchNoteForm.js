import React, { Component } from "react";
import { connect } from "react-redux";

class SearchNoteForm extends Component {
    render() {
        return (
            <div className="search-form">
                <input type="text" ref = { (input) => { this.searchInput = input; } }/>
                <button onClick={ () => this.props.onSearchNote(this.searchInput.value) }>Search Note</button>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onSearchNote(name) {
        dispatch({
            type: 'FIND_NOTE',
            payload: name
        });
    }
});

export default connect(null, mapDispatchToProps, null, { pure: false })(SearchNoteForm);