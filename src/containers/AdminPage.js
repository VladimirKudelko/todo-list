import React, {Component} from "react";
import { connect } from "react-redux";

import { asyncGetUsers, asyncDeleteUser } from "../actions/users";
import { history } from "../components/App";
import path from "../img/cross.png";

class UserPage extends Component {
    componentWillMount() {
        this.props.onGetUsers();
    }

    render() {
        return (
            <div className="admin-page">
                <h1>Hello { this.props.currentUser.username }</h1>
                <button onClick={ this.props.onLogOut }>Log Out</button>
                <h1>List Users Here</h1>
                <div className='usersList'>
                    <button onClick={ this.props.onGetUsers }>Get Users</button>
                    <ul> 
                        {
                            this.props.users.map((user) =>
                                <li key={ user._id /* Mongo _id */ }>
                                    { user.username }
                                    <div className="cross" onClick={ () => this.props.onDeleteUser(user._id) /* Mongo _id */ }>
                                        <img src={ path }/>
                                    </div>
                                </li>
                            ).reverse()
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    users: state.listUsers.users
});

const mapDispatchToProps = dispatch => ({
    onGetUsers() {
        dispatch(asyncGetUsers());
    },
    onDeleteUser(userId) {
        dispatch(asyncDeleteUser(userId));
    },
    onLogOut() {
        localStorage.removeItem('token');

        dispatch({
            type: 'DELETE_AUTH_USER'
        });
        
        history.push('/');
    }
});

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(UserPage);