import React, { Component } from "react";
import { connect } from "react-redux";

import { asyncGetNotes, asyncAddNote, asyncDeleteNote } from "../actions/notes";
import path from "../img/cross.png";

class AddFormNote extends Component {
    componentWillMount() {
        this.props.onGetNotes();
    }

    render() {
        return (
            <div className="add-note-form">
                <input type="text" ref={ (input) => { this.noteInput = input; } }/>
                <button onClick={ () => this.props.onAddNote({ _id: Date.now().toString().slice(-5), name: this.noteInput.value}) }>Add Note</button>
                <button onClick={ this.props.onGetNotes }>Get Notes</button>
                <ul> 
                    {
                        this.props.notes.map((note) =>
                            <li key={ note._id /* Mongo _id */}>
                                { note.name }
                                <div className="cross" onClick={ () => this.props.onDeleteNote(note._id) /* Mongo _id */}>
                                    <img src={ path }/>
                                </div>
                            </li>
                        )
                    }
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    notes: state.notelist.notes.filter((note) => {
        return note.name.includes(state.filterNotes)
    })
});

const mapDispatchToProps = dispatch => ({
    onAddNote(note) {
        if (note.name === '')
            return;
            
        dispatch(asyncAddNote(note));
    },
    onDeleteNote(noteId) {
        if (!noteId)
            return;

        dispatch(asyncDeleteNote(noteId));
    },
    onGetNotes() {
        dispatch(asyncGetNotes());
    }
});

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(AddFormNote);