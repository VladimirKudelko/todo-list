import React, {Component} from "react";
import { connect } from "react-redux";

import { asyncGetUsers, asyncDeleteUser } from "../actions/users";
import { history } from "../components/App";
import AddNoteForm from "./AddNoteForm";
import SearchNoteForm from "./SearchNoteForm";

class UserPage extends Component {
    render() {
        return (
            <div className="user-page">
                <h1>Hello { this.props.currentUser.username }</h1>
                <button onClick={ this.props.onLogOut }>Log Out</button>
                <h1>Organize your tasks</h1>
                <AddNoteForm />
                <SearchNoteForm/> 
            </div>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.currentUser
});

const mapDispatchToProps = dispatch => ({
    onGetUsers() {
        dispatch(asyncGetUsers());
    },
    onDeleteUser(userId) {
        dispatch(asyncDeleteUser(userId));
    },
    onLogOut() {
        localStorage.removeItem('token');
        dispatch({
            type: 'DELETE_AUTH_USER'
        });
        history.push('/');
    }
});

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(UserPage);