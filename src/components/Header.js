import React, { Component } from "react";
import { Link } from "react-router-dom";

const Header = () => {
    return (
        <header>
            <div className="buttons-reg-auth">
                <Link to="/login"><button>Log In</button></Link>
                <Link to="/signup"><button>Sign Up</button></Link>
                <Link to="/admin"></Link>
                <Link to="/user"></Link>
            </div>
        </header>
    );
}

export default Header;