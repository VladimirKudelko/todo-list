import React from 'react';

const HomePage = () => {
    return (
        <div className="home-page">
            <h1>
                Organize your tasks
                And enjoy life ...
            </h1>
            <p>
                Life can seem daunting, but it does not have to be that way. Todoist allows you to track all tasks in one place, so that it's easier for you to 
                carry out your plans, keeping peace of mind.
            </p>
        </div>
    )
}

export default HomePage;