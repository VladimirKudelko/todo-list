import React from "react";
import { Router, Route, Redirect } from "react-router-dom";
import createBrowseHistory from "history/createBrowserHistory";

import UserPage from "../containers/UserPage";
import AdminPage from "../containers/AdminPage";
import Header from "./Header";
import HomePage from "./HomePage";
import LogInPage from "../containers/LogInPage";
import SignUpPage from "../containers/SignUpPage";

export const history = createBrowseHistory();

const App = () => {
    return (
        <Router history={history}>
            <div>
                <Header/>
                <Route exact path="/" component={ HomePage } />
                <Route path="/login" component={ LogInPage } />
                <Route path="/signup" component={ SignUpPage } />
                <Route path="/user" component={ UserPage } />
                <Route path="/admin" component={ AdminPage } />
            </div>
        </Router>
    );
};

export default App;