const css = require('./app.scss');

import React from "react";
import { render } from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension"; // he can have more than 1 devtool
import thunk from "redux-thunk";

import App from "./components/App";
import rootReducer from "./reducers/index";

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

render(
    <Provider store={store}>
        <App/>
    </Provider>, 
    document.getElementById('root')
);