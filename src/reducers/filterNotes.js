const initialState = '';

export default function filterNotes(state = initialState, action) {
    switch(action.type) {
        case 'FIND_NOTE':
            return action.payload
        default: return state;
    }
}