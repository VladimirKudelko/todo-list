const initialState = {};

export default function currentUser(state = initialState, action) {
    switch(action.type) {
        case "ADD_AUTH_USER": 
            return action.payload;  
        case "DELETE_AUTH_USER":
            return '';
        default: return state;
    }
}