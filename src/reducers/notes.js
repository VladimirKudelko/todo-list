import clone from "clone";

const initialState = {
    notes: []
};

export default function notelist(state = initialState, action) {
    let oldState = clone(state);

    switch(action.type) {
        case "ADD_NOTE":
            return {
                oldState,
                notes: [...state.notes, action.payload] 
            };                
        case "DELETE_NOTE":
            return {
                oldState,
                notes: [...state.notes.filter((item) => { return item.id !== action.payload })]
            }
        case "FETCH_NOTES_SUCCESS": 
            return {
                oldState, 
                notes: [...state.notes, ...action.payload]
            };
        case "CLEAR_NOTES":
            return {
                oldState,
                notes: []
            }
        default: return state;
    }
}