import { combineReducers } from "redux";

import notelist from "./notes";
import filterNotes from "./filterNotes";
import listUsers from "./users";
import currentUser from "./user";

const rootReducer = combineReducers({
    notelist,
    filterNotes,
    listUsers,
    currentUser
});

export default rootReducer;