import clone from "clone";

const initialState = {
    users: []
};

export default function listUsers(state = initialState, action) {
    let oldState = clone(state);

    switch(action.type) {
        case "DELETE_USER":
            return {
                oldState,
                users: [...state.users.filter((item) => { return item.id !== action.payload })]
            }
        case "FETCH_USERS_SUCCESS": 
            return {
                oldState, 
                users: [...state.users, ...action.payload]
            };
        case "CLEAR_USERS":
            return {
                oldState,
                users: []
            }
        default: return state;
    }
}