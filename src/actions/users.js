import api from "../api/index";
import { history } from "../components/App";

export const asyncSignUp = (data) => (dispatch) => {
    api.signUpUser(data)
        .then(data => {
            if (data.data.errorMessage) {
                alert (data.data.errorMessage);
                return;
            }
            
            let token = data.data.token;
            let user  = data.data.user;

            token ? 
                localStorage.setItem('token', token) : 
                console.log('token not found');

            dispatch({
                type: 'ADD_AUTH_USER',
                payload: user
            });

            history.push('/user');
        })
        .catch(err => {
            console.log(err);
        })
}

export const asyncLogIn = (data) => (dispatch) => {
    api.logInUser(data)
        .then(data => {
            if (data.data.errorMessage) {
                alert(data.data.errorMessage);
                return;
            }

            let user  = data.data.user;
            let token = data.data.token;

            token ? 
                localStorage.setItem('token', token) : 
                console.log('token not found');

            if (user.isAdmin) {
                dispatch({
                    type: 'ADD_AUTH_USER',
                    payload: user
                });

                history.push('/admin', user);
            } else {
                dispatch({
                    type: 'ADD_AUTH_USER',
                    payload: user
                });

                history.push('/user', user);
            }
        })
        .catch(err => {
            console.log(err);
        });
}

export const asyncLogInGoogle = () => (dispatch) => {
    api.logInUserGoogle()
        .then(data => {
            console.log("Data on client: " + data);
        })
        .catch(err => {
            console.log(err);
        });

}

export const asyncGetUsers = () => (dispatch) => {
    api.usersList()
        .then(( data ) => {
            if (data.data.errorMessage) {
                data.data.errorMessage === "ACCESS DENIED" ? // по статусу ошибки обработка
                    history.push('/') : '';

                alert(data.data.errorMessage);
                
                return;
            }

            console.log("I got users");

            dispatch({
                type: 'CLEAR_USERS'
            });
            dispatch({
                type: 'FETCH_USERS_SUCCESS',
                payload: data.data
            });
        })
        .catch(err => {
            alert(err);
        });
}

export const asyncDeleteUser = (userId) => (dispatch) => {
    api.deleteUser(userId)
        .then((data) => {
            if (data.data.errorMessage) {
                data.data.errorMessage === "ACCESS DENIED" ? 
                    history.push('/') : 
                    '';

                alert(data.data.errorMessage);
                
                return;
            }

            console.log(`delete user with id = ${userId}`);
            
            dispatch({
                type: 'DELETE_USER',
                payload: userId
            })
        })
        .catch(err => {
            console.log(err);
        });
} 