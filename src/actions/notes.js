import api from "../api/index";
import { history } from "../components/App";

export const asyncGetNotes = () => (dispatch) => {
    api.listNotes()
        .then((data) => {
            if (data.data.errorMessage) {
                data.data.errorMessage === "ACCESS DENIED" ? 
                    history.push('/') : 
                    '';

                alert(data.data.errorMessage);
                
                return;
            }

            console.log("I got notes");

            dispatch({ type: 'CLEAR_NOTES' });
            dispatch({
                type: 'FETCH_NOTES_SUCCESS',
                payload: data.data
            });
        })
        .catch(err => {
            console.log(err);
        });
}

export const asyncAddNote = (note) => (dispatch) => {
    api.createNote(note)
        .then((data) => {
            if (data.data.errorMessage) {
                data.data.errorMessage === "ACCESS DENIED" ? 
                    history.push('/') : 
                    '';

                alert(data.data.errorMessage);
                
                return;
            }

            console.log('add note');

            dispatch({
                type: 'ADD_NOTE',
                payload: note
            });
        })
        .catch(err => {
            console.log(err);
        })
}

export const asyncDeleteNote = (noteId) => (dispatch) => {
    api.deleteNote(noteId)
        .then((data) => {
            if (data.data.errorMessage) {
                data.data.errorMessage === "ACCESS DENIED" ? 
                    history.push('/') : 
                    '';

                alert(data.data.errorMessage);
                
                return;
            }
            
            console.log(`delete note with id = ${noteId}`);

            dispatch({
                type: 'DELETE_NOTE',
                payload: noteId
            })
        })
        .catch(err => {
            console.log(err);
        });
}